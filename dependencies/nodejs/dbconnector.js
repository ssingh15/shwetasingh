const AWS = require('aws-sdk');
var ssm = new AWS.SSM();

exports.getConnectionParams =  async(event) => {
    if(process.env.ENVIRONMENT === 'Production'){
        var params = {
            "Name": event.param_name, 
            "WithDecryption": true
          };
          
          try{
            const data = await ssm.getParameter(params).promise();
            return {response:200, payload: JSON.parse(data.Parameter.Value)};
          }catch (err){
            console.log("Got error:", err);
            const error = new Error(`Cannot get Parameters from SSM Parameter store : ${err.code}`);
            error.code = 'NO_SSM_GET';
            throw error;
          }    
    }else{
        try{
          const connection = require('./dbconnection/dbconnection.json');
          console.log(" NON PRODUCTION ENVIRONMENT ");
          return {response:200, payload: connection};
        }catch(err){
          console.log(" NON PRODUCTION ENVIRONMENT ");
          if(err.code === 'MODULE_NOT_FOUND'){
            console.log("!! DB Connection parameter file not found: In order to use getConnectionParams() in a non-production environment (dev, test etc) \
                         you must have your conection parameters as a .json file in a directory named /dbconnection at the project's \
                         root directory for local testing !!",process.env.ENVIRONMENT);
            const error = new Error("DB Connection parameter file not found.");
            error.code = 'NO_PARAM_FOUND';
            throw error;
          }
        }
    }
};