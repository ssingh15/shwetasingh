'use strict';

const setupWatchdogTimer = (context, respCode, message) => {
  // When timer triggers, emit an event to the 'myErrorHandler' function
 
  const timeoutHandler = () => {
    // Include original event and request ID for diagnostics
    console.log(`Error:${context.functionName} Function timed out with error code:${respCode}~${message}`); //logs to CloudWatch

    const response = { responseCode: respCode, responseMessage: message };
    return {
      "isBase64Encoded": false,
      "statusCode": respCode,
      "body": JSON.stringify(response),
      "headers": {
         'Content-Type': 'application/json',
         'Access-Control-Allow-Origin' : '*'
         }
    }; //response to triggering/invoking service
  };
 
  // Set timer so it triggers one second before this function would timeout
  setTimeout(timeoutHandler, context.getRemainingTimeInMillis() - 1000); 
}
 
module.exports = {
    setupWatchdogTimer
};