#!/bin/bash
echo "***********************************************"
echo ' _                    _     _             _    
| |    ___   ___ __ _| |___| |_ __ _  ___| | __
| |   / _ \ / __/ _` | / __| __/ _` |/ __| |/ /
| |__| (_) | (_| (_| | \__ \ || (_| | (__|   < 
|_____\___/ \___\__,_|_|___/\__\__,_|\___|_|\_\
                                               '
echo "***********************************************"
PID=0
trap stop INT

function stop {
    if [[ "${PID}" -gt 0 ]]; then
        echo "# Exiting and terminating container. Please wait..."        
        sleep 10
        docker-compose down
        exit 0
    else
        echo "# Exiting "
        exit 0
    fi
}

state=`docker-compose ps|grep localstack|awk '{print $3;}'`
if [[ $state == "Up" ]]; then
    echo "Shutting Down Container"
    docker-compose down
    exit 0
fi

if [[ `ls -l ./tablespecs | wc -l` -gt 0 ]] || [[ `ls -l ./bucketspecs | wc -l` -gt 0 ]]; then
    if [[ "$OSTYPE" == "darwin"* ]]; then
        echo "# Starting Localstack Container (Press Ctrl+C to terminate)"
        TMPDIR=/private$TMPDIR docker-compose up > logfile.log 2>&1 &
        PID=$!
    else
        echo "# Starting Localstack Container (Press Ctrl+C to terminate)"
        docker-compose up -d  
        PID=$!  
    fi

    echo "# Starting services"

    ( tail -f -n0 logfile.log & ) | grep -q "Ready."
    echo "End" >> logfile.log

    id=`docker ps | grep localstack/localstack | awk '{print $1;}'`
    echo "# Container id is $id"

    docker_host=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${id}`

    docker exec -it ${id} /opt/code/localstack/lspostscript.sh

    echo "# Cleaning up"
    rm -rf logfile.log
    echo "# Container is in sam network. If you intend to run API Gateway "
    echo "# please run the sam-cli command below:"
    echo "# sam local start-api --docker-network sam"
    echo "# You can use awslocal-cli too - https://github.com/localstack/awscli-local"
    echo "# Please ensure that you have envirnment variable LOCALSTACK_HOST set to 0.0.0.0"
    echo "# To view Localstack console go to http://0.0.0.0:8080 in your browser"
    echo "# The S3 endpoint must be set to http://${docker_host}:4572/{bucket_name} for local testing inside Lambda for S3"
    echo "# Also NOTE: for Local testing to work for S3 s3ForcePathStyle must be set to true "
    echo "# example: const s3 = new AWS.S3({region: 'us-east-1', endpoint: 'http://172.29.0.2:4572/', s3ForcePathStyle: true});"
else
    echo "# No Serverless Resources to create. Please continue to use SAM-CLI."
fi

exit 0
