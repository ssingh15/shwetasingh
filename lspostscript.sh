#!/bin/bash

if [[ `ls -l /opt/code/localstack/tablespecs | wc -l` -gt 0 ]]; then
    echo "# Creating DynamoDB Table"
    # awslocal dynamodb create-table --table-name tasks --attribute-definitions AttributeName=Id,AttributeType=N --key-schema AttributeName=Id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5
    FILES=/opt/code/localstack/tablespecs/*
    for file in $FILES
    do  
        awslocal dynamodb create-table --cli-input-json file://$file  
    done
fi

if [[ `ls -l /opt/code/localstack/bucketspecs | wc -l` -gt 0 ]]; then
    buckets=`cat /opt/code/localstack/bucketspecs/buckets.json | python -c "import sys, json; print(','.join(str(r) for r in json.load(sys.stdin)['s3-buckets']))"`
    IFS=', ' read -r -a bucketarray <<< $buckets

    if [[ "${#bucketarray[@]}" -gt 0 ]]; then
        echo "# Creating S3 Buckets"
        for bucket in "${bucketarray[@]}"
        do
            # echo "$bucket"
            awslocal s3 mb s3://"$bucket"
        done
    fi
fi
