'use strict';
const axios = require('axios');
const secrets = require("./getSecrets")

exports.handler = async (event, context) => {
  console.log('event######',event)
  let response ={  
    "isBase64Encoded" : false,
    "statusCode": 200,
    "headers": {"Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"},
    "body": ""
    
  }
  let clientId = await secrets.getSecrets(process.env.CLIENT_ID);
  const params=event.queryStringParameters
  try{
    const product_id=event.pathParameters.product_id
    let productUrl = `${process.env.DETAIL_URL}/${product_id}?client_id=${clientId}`
    if(params){
      productUrl= `${productUrl}&expand=${params.expand}`
    }
    console.log('product Deatil URL######',productUrl)
    const res = await axios.get(productUrl)
    const imgObj = await axios.get(`https://partycity6.scene7.com/is/image/PartyCity/${product_id}_set?req=set,json`)
    console.log('res#####',res.data)
    console.log('imgobj#####',imgObj.data.substring(imgObj.data.indexOf("("),imgObj.data.length)  )
    var imgList = imgObj.data.substring(imgObj.data.indexOf("("),imgObj.data.length) 
    imgList =  imgList.substring(1, imgList.length-5);
    imgList= JSON.parse(imgList)["set"]["item"]
     var imgArr=[]
    console.log('typeof imgList',  Array.isArray(imgList))
    if(Array.isArray(imgList)){
        imgList.forEach(img=> {
         imgArr.push(`https://partycity6.scene7.com/is/image/${img.i.n}`)}
        )
     }else{
  
     imgArr.push(`https://partycity6.scene7.com/is/image/${imgList.i.n}`)
    
    }
    res.data["bb_img"] = imgArr
    response.body=JSON.stringify(res.data)
    console.log('response#######',response) 
    return response
  }catch(err){
      response.statusCode= 400
      response.body=err
     console.log('response@@@@@@',response)
    return response
  }
};


