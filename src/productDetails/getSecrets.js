var AWS = require('aws-sdk'),
    region = "us-east-1"

// Create a Secrets Manager client
var client = new AWS.SecretsManager({
    region: region
});

function getAwsSecret(secretName) {
    console.log('secretName########',secretName)
    return client.getSecretValue({ SecretId: secretName }).promise();
  }
  
  async function getAwsSecretAsync (secretName) {
    try {
      const response = await getAwsSecret(secretName);
      //console.log('response######',response)
      if ('SecretString' in response) {
        return response.SecretString;
        }
      return response;
    } catch (error) {
      console.error('Error occurred while retrieving AWS secret');
      console.error(error);
    }
  }

module.exports.getSecrets  = async(secretName)=>{
   const response = await getAwsSecretAsync(secretName)
   console.log('response#######',response)
   return response
}
  