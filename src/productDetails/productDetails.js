'use strict';
const axios = require('axios');
const secrets = require("./getSecrets")

exports.handler = async (event, context) => {
  console.log('event######',event)
  const listofskus=event.pathParameters.listofskus
  const params=event.queryStringParameters
  let response ={  
    "isBase64Encoded" : false,
    "statusCode": 200,
    "headers": {"Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"},
    "body": "body"
  }
 
  let detailURL = process.env.DETAIL_URL
  let clientId = await secrets.getSecrets(process.env.CLIENT_ID);
  try{
    let productUrl = `${detailURL}/${listofskus}?client_id=${clientId}`
    if(params){
      productUrl= `${productUrl}&expand=${params.expand}`
    }
    console.log('product Deatils URL######',productUrl)
    const res = await axios.get(productUrl)
    response.body=JSON.stringify(res.data)
    console.log('response######',response)
    return response
  }catch(err){
    response.body=err
    return response
  }  

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};