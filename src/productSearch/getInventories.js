const fs = require('fs');
const axios = require('axios');
const https = require('https');
const secrets = require('./getSecrets')
const keyName = "token-key"
const certName = "token-cert"

module.exports.getInvertories = async (event, token, xmlBody) =>{
  
  let credentials = await secrets.getSecrets(process.env.USER_CREDENTIALS);
    console.log('credentials#######',credentials)
    const key = await secrets.getSecrets(keyName);
    const cert = await secrets.getSecrets(certName);
    console.log('key####1',key, 'cert####1', cert, "credentials.LoginID",JSON.parse(credentials).LoginID)
    var response=  await axios.request({
      url: `${process.env.INVENTORY_URL}?_loginid=${JSON.parse(credentials).LoginID}&_token=${token}`,
      method: 'post',
      headers: {
          "content-type": "application/xml",
      },
      data: xmlBody
     
    ,
      httpsAgent: new https.Agent({
        cert:cert,
        key:key,
        passphrase: 'ibmkey',
        //pfx: fs.readFileSync(cert),
      })
    })
    return response.data
  }
  
  module.exports.createToken = async (event) => {
    let credentials = await secrets.getSecrets(process.env.USER_CREDENTIALS);
    console.log('credentials####',credentials)
    const key = await secrets.getSecrets(keyName);
    const cert = await secrets.getSecrets(certName);
    console.log('key####',key, 'cert####', cert)
    var res = await axios.request({
      url: process.env.TOKEN_URL,
      method: 'post',
      headers: {
          "content-type": "application/json",
      },
      data: credentials
     
  ,
      httpsAgent: new https.Agent({
        cert:cert,
        key:key,
        passphrase: 'ibmkey',
        //pfx: fs.readFileSync(`./${cert}`/*./owner=API_user_Marketplace_QA.p12'*/),
      })
  } )
  return res.data.UserToken
  }
  //export default getInvertories
  //export default createToken