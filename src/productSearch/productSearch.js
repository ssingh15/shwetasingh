'use strict';
const {getInvertories, createToken} = require("./getInventories");
const products = require("./productSearchDetails")
const updateQty = require("./addOnHandQty")

var Redis = require("ioredis");
var client = new Redis({
  port: "6379",  
  host: "bb-token.0phwet.ng.0001.use1.cache.amazonaws.com",  
  db: 0
});
console.log('client####',client)

exports.handler = async (event, context) => {
  console.log('event#####',event,'context######',context)
  let response ={  
    "isBase64Encoded" : false,
    "statusCode": 200,
    "headers": {"Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"},
    "body": "body"
    
  }
  const params = event.queryStringParameters
  const skus= await products.productSearchDetails(params)
  var updatedproducts=skus
  var store_id = params.store
  console.log("skus $$$$$$$$",skus)
  var product = skus.Products.hits
  var i=1
  var xmlBodyStr =`<IVNodePromise>
	<IVNodePromiseLines>`
  if(product){
    product.forEach(ele=>{
    
      xmlBodyStr=xmlBodyStr+`<IVNodePromiseLine DeliveryMethod="PICK" LineId="${i}" ItemID="${ele.product_id}" ProductClass="GOOD" UnitOfMeasure="EACH">
        <ShipNodes>
          <ShipNode Node="${store_id}" />
        </ShipNodes>
      </IVNodePromiseLine>`
      i++
    })
  }
  
  if(params.listOfSkus){
    var listOfSkus = skus.ListOfSkus.data 
    
    listOfSkus.forEach(ele=>{
      i++
    xmlBodyStr=xmlBodyStr+`<IVNodePromiseLine DeliveryMethod="PICK" LineId="${i}" ItemID="${ele.product_id}" ProductClass="GOOD" UnitOfMeasure="EACH">
			<ShipNodes>
				<ShipNode Node="${store_id}" />
			</ShipNodes>
		</IVNodePromiseLine>`
		
  })
  }
  xmlBodyStr=xmlBodyStr+`</IVNodePromiseLines>
  </IVNodePromise>`
  try{
    let token = await client.get("token");
    console.log('token#####',token)
    var invetoryResponse=  await getInvertories(params, token, xmlBodyStr)
    console.log('invetoryResponse#######',invetoryResponse)
    updatedproducts = updateQty.addOnHandQty(skus, invetoryResponse)
    
    response.statusCode=200
    response.body = JSON.stringify(updatedproducts)
    return response
  }catch(err){
    var newToken = await createToken(params)
    var invetoryResponse=  await getInvertories(params, newToken, xmlBodyStr)
    console.log('invetoryResponse#######',invetoryResponse)
    await client.set("token", newToken);
    updatedproducts = updateQty.addOnHandQty(skus, invetoryResponse)
    console.log('updatedproducts######',updatedproducts)
    response.statusCode=200
    response.body = JSON.stringify(updatedproducts)
   
  }
  console.log('return response######',response)
  return response
    
};





