const fs = require('fs');
var axios = require('axios').default;
const https = require('https');
const secrets = require("./getSecrets")
module.exports.productSearchDetails = async (event)=>{
  console.log('event productSearchDetails#########',event)
  let clientId = await secrets.getSecrets(process.env.CLIENT_ID);
  console.log("clientId####@@",clientId)
  console.log('searchURL#######$$$$',process.env.SEARCH_URL)
  var  builderStepParam  = event.refine_3
  if(event.refine_1 && event.refine_1.toLowerCase().includes("builderStep".toLowerCase())) builderStepParam=event.refine_1
  if(event.refine_2 && event.refine_2.toLowerCase().includes("builderStep".toLowerCase())) builderStepParam=event.refine_2
  //if(event.refine_3 && event.refine_4.length>0 && event.refine_4.toLowerCase().includes("builderStep".toLowerCase())) builderStepParam=event.refine_4

  var builderStep = builderStepParam.length>0 ? builderStepParam.substring(builderStepParam.indexOf('='),builderStepParam.length ).replace('=','') : 'Curated'
  console.log('builderStep######',builderStep)
  var ProductDetails, response={}
    
    try{
      var baseURL= `${process.env.SEARCH_URL}?client_id=${clientId}`
      for(const [key,value] of Object.entries(event)){
          if(value && value!=='env'){
            baseURL=`${baseURL}&${key}=${value}`
          }
      }
      console.log('baseURL########',baseURL)
      const res = await axios.get(baseURL).catch(err =>{
        console.log(err.code);
        console.log(err.message);
        console.log(err.stack);
      })
      console.log("res#######",res)
      var productSearchResponse=[];
      console.log("res.data.hits#######",res.data.hits)
      if( res.data.hits){
        res.data.hits.forEach(ele=>{
          //update c variant   values
          if(ele.hit_type==='master' && ele.c_variants){
            ele.c_variants.forEach(ele1=>{
              ele1['price'] = ele1['list_price']; // Assign new key
              delete ele1['list_price']
              productSearchResponse.push(ele1)
            })
            
          }else{
            productSearchResponse.push(ele)
          }
          //console.log('ele###',ele)
        })
        console.log("productSearchResponse#######",productSearchResponse)
      res.data.hits=productSearchResponse
      res.data.hits.forEach(ele=>{
        
        var bb_filters =[]
        ele["c_heliumCost"]=ele.c_heliumCost || 0.0
        ele["c_airCost"]= ele.c_airCost || 0.0
        var totalPrice = event.inflation_type==='a' ? (ele.price+ele.c_airCost).toFixed(2)
        : event.inflation_type==='h' ? (ele.price+ele.c_heliumCost).toFixed(2)
        : +ele.price;
        ele["bb_totalPrice"] = +totalPrice
        
        for(const [key, value] of Object.entries(ele)){
          if(builderStep.toLowerCase()==="finish" ){
            if(key==='c_productType' &&  value.length>0){
              bb_filters=value.split('|')
            }
            
          }else{
            if(key==='c_specialityBalloon' && value.length>0){
              bb_filters=value.split('|')
            }
          }
          
        }
        ele['bb_filters']=bb_filters
    })

  }
    response["Products"]=res.data
    console.log('response!!!!!!!',response)
      if( event.listOfSkus && event.listOfSkus.length>0){
        var listOfSkus=event.listOfSkus.replace(/[()]/g, '')
        listOfSkus = listOfSkus.split(',')
        var obj={}
        listOfSkus.forEach(ele=>{
          var str1=ele.split(':')
          obj[str1[0]] =+str1[1]||0
        })
        listOfSkus=Object.keys(obj).join(',')
        console.log('details######',process.env.DETAIL_URL)
        ProductDetails = await axios.get(`${process.env.DETAIL_URL}/(${listOfSkus})?client_id=${clientId}`)
        console.log('ProductDetails["data"]',ProductDetails)
        if( typeof ProductDetails.data["data"] !== 'undefined'){
        var productDetailData=ProductDetails.data["data"]
        var detailData=[]
        productDetailData.forEach(ele=>{
              var bb_filters =[]
          for(const [key, value] of Object.entries(ele)){
            if(key==='c_specialityBalloon' && value.length>0){
              bb_filters=value.split('|')
            }
          }
          ele['bb_filters']=bb_filters
            for (const [key, value] of Object.entries(obj)){
              if(key===ele.id){
                ele["bb_qty"]=value
              }
            }
            detailData.push({
            "product_id" : ele.id,
            "product_name": ele.name || "",
            "price": ele.c_regPrice,
            "c_airCost": ele.c_airCost || 0.0,
            "c_heliumCost":  ele.c_heliumCost || 0.0,
             "c_shortDescription2": ele.c_shortDescription2 || "",
            "c_specialityBalloon": ele.c_specialityBalloon || "",
            "bb_qty": ele.bb_qty,
            "bb_filters": bb_filters
            })
           
        })
        ProductDetails.data["data"]=detailData
        //console.log('detailData####',ProductDetails.data)
        response["ListOfSkus"]=ProductDetails.data
      }
    }
    console.log("response########",response)
    return response 
    }catch(err){
      return `err: ${err}`
    }
  }
  