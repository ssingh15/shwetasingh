'use strict';

const axios = require('axios');
const secrets = require("./getSecrets")

exports.handler = async (event, context) => {
  console.log('event######',event)
  let response ={
    "isBase64Encoded" : false,
    "statusCode": 200,
    "headers": {"Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"},
    "body": "body"
  }
  let clientId = await secrets.getSecrets(process.env.CLIENT_ID);
  var baseURL= `${process.env.STORE_URL}?client_id=${clientId}`
      for(const [key,value] of Object.entries(event.queryStringParameters)){
          if(value && key !== "env"){
            baseURL=`${baseURL}&${key}=${value}`
          }
      }
      console.log('Store URL ########',baseURL)
  //postal_code=${event.postal_code}&country_code=${event.country_code}&distance_unit=${event.distance_unit}&count=${event.count}&max_distance=${event.max_distance}
  try{
    const res = await axios.get(baseURL)
    console.log("res######",res)
    response.body=JSON.stringify(res.data)
  }catch(err){
    response.body=err
  }
  return response
}